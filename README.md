# dee
Copy a command's stdout and stderr to separate files. Like `tee`, but for stdout *and* stderr.

~~~shell
go get github.com/gima/dee
~~~

## Example

Usage example:

~~~shell
$ dee -- bash -c 'echo hello>&1; echo world>&2'
hello
world

$ cat stdout
hello

$ cat stderr
world
~~~

Command's help:

~~~shell
Usage: dee [arguments] <Command to execute and its arguments..>

Arguments:
  -err string
        Filename to write stderr to (default "stderr")
  -out string
        Filename to write stdout to (default "stdout")

Hint: Prepend command with '--' to avoid it's arguments from being
      processed by this program.
~~~

## License

MIT

*go, golang*

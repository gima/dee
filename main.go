package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	stdOutPath := flag.String("out", "stdout", "Filename to write stdout to")
	stdErrPath := flag.String("err", "stderr", "Filename to write stderr to")
	parseFlags()

	cmd := exec.Command(flag.Args()[0], flag.Args()[1:]...)

	// open files for writing process's output to
	stdOutWriter, err := os.OpenFile(*stdOutPath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0664)
	if err != nil {
		log.Fatalln("Failed to open file for writing stdout to. Error =", err)
	}
	stdErrWriter, err := os.OpenFile(*stdErrPath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0664)
	if err != nil {
		log.Fatalln("Failed to open file for writing stderr to. Error =", err)
	}

	// create writers that duplicate output to said files and this process's stdout & stderr
	outWriter := io.MultiWriter(os.Stdout, stdOutWriter)
	errWriter := io.MultiWriter(os.Stderr, stdErrWriter)

	cmd.Stdout = outWriter
	cmd.Stderr = errWriter
	exitcode := 0

	// execute process and wait until it has terminated
	if err := cmd.Run(); err != nil {
		// something went wrong
		exitcode = 1

		switch t := err.(type) {
		default:
			// error = cmd.Run error
			log.Println("Execution of command failed. Error =", err)

		case *exec.ExitError:
			// error = program exited with none-zero exitcode
			// determine exitcode, if possible (WaitStatus is os-dependant)
			if waitstatus, ok := t.Sys().(syscall.WaitStatus); ok {
				exitcode = waitstatus.ExitStatus()
			}
		}

	}

	// ignore close errors
	stdOutWriter.Close()
	stdErrWriter.Close()

	os.Exit(exitcode)
}

func parseFlags() {
	flag.CommandLine.SetOutput(os.Stdout)
	flag.Usage = func() {
		fmt.Printf("Usage: %s [arguments] <Command to execute and its arguments..>\n\nArguments:\n", os.Args[0])

		flag.PrintDefaults()
		fmt.Println("\nHint: Prepend command with '--' to avoid it's arguments from being processed by this program.")
	}

	flag.Parse()

	if len(flag.Args()) == 0 {
		flag.Usage()
		os.Exit(1)
	}
}
